#!/usr/bin/env python3

import numpy as np

verbose = 2

def read_config(cfile):
    """Read the config"""
    global verbose
    with open(cfile, 'r') as f:
        for l in f:
            cstart = l.find('#')
            if cstart != -1:
                l = l[:cstart]
            if len(l) <= 1: # skip empty lines
                continue
            s = l.split('=')
            key = s[0].strip()
            val = s[1].strip()
            if key == 'sysfile':
                sysfile = val
            elif key == 'repcoeffile':
                repcoeffile = val
            elif key == 'tbpfile':
                tbpfile = val
            elif key == 'verbose':
                verbose = int(val)
            else:
                raise KeyError('Unknown key in config file line: %s'
                               % l.strip())

    systems = []
    with open(sysfile, 'r') as f:
        nsys = int(f.readline().strip())
        for l in f:
            systems.append(l.strip())

    r = []
    with open(repcoeffile, 'r') as f:
        nr = int(f.readline().strip())
        for l in f:
            r.append(float(l.strip()))
    r = np.array(r)
    
    t = {}
    # Ordering important here!
    with open(tbpfile, 'r') as f:
        ts = f.readlines()
    ts = [float(x.strip()) for x in ts]
    t['es'] = ts[0]
    t['ep'] = ts[1]
    t['vsss'] = ts[2]
    t['vsps'] = ts[3]
    t['vpps'] = ts[4]
    t['vppp'] = ts[5]
    t['n'] = int(ts[6]) # n is an integer!
    t['nc'] = ts[7]
    t['rc'] = ts[8]
    t['r0'] = ts[9]
    t['r1'] = ts[10]
    t['phi0'] = ts[11]
    t['m'] = ts[12]
    t['mc'] = ts[13]
    t['dc'] = ts[14]
    t['d0'] = ts[15]
    t['d1'] = ts[16]

    return systems, r, t

def read_xyz(fname):
    """Parse an xyz file"""
    with open(fname) as f:
        natoms = int(f.readline())
        f.readline()
        a = np.empty((natoms, 3))
        for ii, row in enumerate(a):
            s = f.readline().split()
            a[ii, :] = np.array((float(s[1]), float(s[2]), float(s[3])))
    return a

def phi(t, r):
    from math import pow, exp
    return t['phi0'] * pow(t['d0'] / r, t['m']) \
        * exp(t['m'] * (-pow(r / t['dc'], t['mc']) \
                        + pow(t['d0'] / t['dc'], t['mc'])))


def repulsive_energy(tbp, atoms, r):
    from math import pow, sqrt
    erep = 0.
    for ii, rowi in enumerate(atoms):
        for n, rc in enumerate(r):
            phisum = 0.
            for jj, rowj in enumerate(atoms):
                if ii == jj:
                    continue
                v = rowj - rowi
                phisum += phi(tbp, sqrt(v.dot(v)))
            erep += rc * pow(phisum, n)
    return erep

def scaling(t, r):
    from math import pow, exp
    return pow(t['r0'] / r, t['n']) \
        * exp(t['n'] * (-pow(r / t['rc'], t['nc']) \
                        + pow(t['r0'] / t['rc'], t['nc'])))

def construct_hamil(atoms, t):
    """Construct the Hamiltonian"""
    from math import pow, sqrt
    n = 4
    k = n * atoms.shape[0]
    hamil = np.zeros((k, k))
    if verbose > 15:
        hamil[:,:] = 4711.
    for ii in range(k):
        for jj in range(ii + 1):
            # Orbital indices within block
            ki = ii % n
            kj = jj % n
            # Which block
            bi = ii // n
            bj = jj // n
            if verbose > 20:
                print("Block indices: ", bi, bj)
                print("Orbital indices: ", ki, kj)
                print("Indices: ", ii, jj)
            # Diagonal elements
            if ii == jj:
                if ki == 0:
                    hamil[ii, ii] = t['es']
                else:
                    hamil[ii, ii] = t['ep']
            # Off-diagonal elements
            # zero blocks for self-interaction
            elif (ii - jj) > 0 and ii - jj <= n - kj - 1 and ii != jj:
                hamil[ii, jj] = 0.
            # s-s
            elif ki == 0 and kj == 0:
                v = atoms[bj] - atoms[bi]
                d = sqrt(v.dot(v))
                hamil[ii, jj] = scaling(t, d) * t['vsss']
            # s-p and p-s
            elif (ki == 0 and kj > 0) or (kj == 0 and ki > 0):
                if ki > 0:
                    ll = -1.
                else:
                    ll = 1.
                v = atoms[bj] - atoms[bi]
                d = sqrt(v.dot(v))
                hamil[ii, jj] = ll * scaling(t, d) * t['vsps'] \
                                * v[max(ki, kj) - 1] / d
            # px-px, py-py, pz-pz
            elif ki > 0 and ki == kj:
                v = atoms[bj] - atoms[bi]
                d = sqrt(v.dot(v))
                ll = pow(v[ki - 1] / d, 2)
                hamil[ii, jj] = scaling(t, d) \
                                * (ll * t['vpps'] + (1 - ll) * t['vppp'])
            # px-py
            elif (ki > 0 and kj > 0 and ki != kj):
                v = atoms[bj] - atoms[bi]
                d = sqrt(v.dot(v))
                ll = v[ki - 1] / d * v[kj - 1] / d
                hamil[ii, jj] = scaling(t, d) \
                                * (ll * t['vpps'] - ll * t['vppp'])
    # Fill in upper triangular part
    if verbose > 16:
        for ii in range(k):
            for jj in range(ii):
                hamil[jj, ii] = hamil[ii, jj]
    if verbose > 15:
        if len(atoms) < 5:
            print("Hamiltonian:\n", hamil, "\n")
        else:
            with open("hamil.txt", "w") as f:
                f.write(hamil)
    return hamil

def electronic_energy(eig):
    return 2 * eig[:len(eig) // 2].sum()

if __name__ == '__main__':
    import time
    systems, r, tbparams = read_config("tb.conf")
    for sys in systems:
        atoms = read_xyz(sys)
        t1 = time.time()
        rep = repulsive_energy(tbparams, atoms, r)
        t2 = time.time()
        hamil = construct_hamil(atoms, tbparams)
        t3 = time.time()
        eigs = np.linalg.eigvalsh(hamil)
        t4 = time.time()
        eel = electronic_energy(eigs)
        t5 = time.time()
        print('=================================')
        print('Calculating system ', sys)
        print('Constructing Hamiltonian took ', t3-t2, 's.')
        print('Solving eigenvalues took ', t4 - t3, 's.')
        print('Eel = ', eel, ' took ', t5-t4, 's.')
        print('Erep = ', rep, ' took ', t2-t1, 's.')
        print('Etot = ', rep + eel)
        print('Eb/N = ', (rep + eel) / len(atoms) + 1.150976511819100)
