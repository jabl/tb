tb
==

Simple Tight Binding program implemented in Fortran, C++, Julia, and
python. There are two C++ versions, one using the Eigen library and
the other using the Armadillo library.

Each program starts by reading a configuration file called 'tb.conf'
in the current directory.  This file contains the paths of other files
describing systems and parameters needed.  Example configs and
parameters are provided in the root directory of the package (same
directory as this file). Thus, to run e.g. the python implementation
run in the root directory

$ python3 py/tb.py
