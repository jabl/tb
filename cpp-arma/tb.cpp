// Simple C++ Tight Binding code
// Copyright (C) 2009-2014 Janne Blomqvist


#define _GNU_SOURCE 1

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <fenv.h>
#include <cerrno>
#include <algorithm>

#include <armadillo>

using namespace arma;

typedef int index_t; // Type for array sizes and indices

const char* conffile = "tb.conf";

struct config 
{
        std::string sysfile, repcoeffile, tbpfile;
        int verbose;
};

// Tight binding params
struct tbparams 
{
        double es,
                ep,
                vsss,
                vsps,
                vpps,
                vppp,
                n,
                nc,
                rc,
                r0,
                r1,
                phi0,
                m,
                mc,
                dc,
                d0,
                d1;
};

void trapfpe()
{
#ifdef __GNU_LIBRARY__
        /*
         * Enable some exceptions.  At startup all exceptions are masked.  
         */

        // feenableexcept
        // (FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW|FE_INEXACT|FE_UNDERFLOW);
        feenableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW);
#endif
}

void untrapfpe()
{
#ifdef __GNU_LIBRARY__
	// Disable the previous set of exception traps. Must be called
	// before entering LAPACK.
	fedisableexcept(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW);
#endif
}


template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}


// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

config read_config()
{
        config cf;
        std::ifstream inp(conffile, std::ifstream::in);

        if (!inp.is_open()) 
        {
                std::ofstream op(conffile);
                op << "sysfile=systems.txt\n";
                op << "repcoeffile=repcoef.txt\n";
                op << "tbpfile=tbparams.txt\n";
                op << "verbose=2" << std::endl;
                op.close();
                inp.open(conffile, std::ifstream::in);
        }

        std::string line;
        while (getline(inp, line)) 
        {
            auto i = line.find("#");
            if (i != std::string::npos)
                line = line.substr(0, i);
            if (line.length() <= 1)
                continue;
            auto s = split(line, '=');
            if (s.size() != 2) {
                std::cout << "Invalid syntax in config file line: " << line << "\n";
                exit(1);
            }
            auto key = trim_copy(s[0]);
            auto val = trim_copy(s[1]);
            if (key.find("sysfile") != std::string::npos)
                cf.sysfile = val;
            else if (key.find("repcoeffile") != std::string::npos)
                cf.repcoeffile = val;
            else if (key.find("tbpfile") != std::string::npos)
                cf.tbpfile = val;
            else if (key.find("verbose") != std::string::npos)
            {
                std::istringstream iss(val);
                iss >> cf.verbose;
            }
        }
        inp.close();
        return cf;
}


std::vector<double> read_repcoef(const std::string& fname)
{
        std::ifstream inp(fname.c_str(), std::ifstream::in);
        if (!inp.is_open()) 
        {
                std::cerr << "Could not open " << fname << ", error message: ";
                std::cerr << strerror(errno) << std::endl;
                exit(errno);
        }
        int ncoef;
        inp >> ncoef;
        std::vector<double> v;
        v.reserve(ncoef);
        for (int i = 0; i < ncoef; ++i)
        {
                double d;
                inp >> d;
                v.push_back(d);
        }
        inp.close();
        return v;
}

tbparams read_tbparams(const std::string& fname)
{
        tbparams t;
        std::ifstream inp(fname.c_str(), std::ifstream::in);
        if (!inp.is_open()) 
        {
                std::cerr << "Could not open " << fname << ", error message: ";
                std::cerr << strerror(errno) << std::endl;
                exit(errno);
        }
        inp >> t.es >> t.ep >> t.vsss >> t.vsps >> t.vpps >> t.vppp >> t.n 
            >> t.nc >> t.rc >> t.r0 >> t.r1 >> t.phi0 >> t.m >> t.mc >> t.dc 
            >> t.d0 >> t.d1;
        inp.close();
        return t;
}

std::vector<std::string> read_systems(const std::string& fname)
{
        std::ifstream inp(fname.c_str(), std::ifstream::in);
        if (!inp.is_open()) 
        {
                std::cerr << "Could not open " << fname << ", error message: ";
                std::cerr << strerror(errno) << std::endl;
                exit(errno);
        }
        int nsys;
        inp >> nsys;
        std::string line;
        getline(inp, line); // Get rid of the rest of the first line.
        std::vector<std::string> v;
        v.reserve(nsys);
        for (int i = 0; i < nsys; i++) 
        {
                getline(inp, line);
                v.push_back(line);
        }
        inp.close();
        return v;
}

mat read_xyz(const std::string& fname, const config& conf)
{
        std::ifstream inp(fname.c_str(), std::ifstream::in);
        if (!inp.is_open()) 
        {
                std::cerr << "Could not open " << fname << ", error message: ";
                std::cerr << strerror(errno) << std::endl;
                exit(errno);
        }
        int natoms;
        inp >> natoms;
        std::string dummy;
        getline(inp, dummy); // Rest of 1st line.
        getline(inp, dummy); // skip comment line
        mat m(natoms, 3);
        for (int i = 0; i < natoms; i++) 
        {
                inp >> dummy >> m(i, 0) >> m(i, 1) >> m(i, 2);
                getline(inp, dummy); // Stuff at end of line
        }
        inp.close();
        if (conf.verbose > 15 && natoms < 10)
                std::cout << "Coordinates:\n" << m << "\n";
        return m;
}


double phi(const tbparams& t, double r)
{
        return t.phi0 * pow(t.d0 / r, t.m)
                * exp(t.m * (-pow(r / t.dc, t.mc)
                             + pow(t.d0 / t.dc, t.mc)));
}

double repulsive_energy(const tbparams& t, 
                        const mat& atoms,
                        const std::vector<double>& repcoefs)
{
        double erep = 0.0;
        for (index_t i = 0; i < (index_t) atoms.n_rows; i++) 
        {
                for (size_t n = 0; n < repcoefs.size(); n++) 
                {
                        double phisum = 0.0;
                        for (index_t j = 0; j < (index_t) atoms.n_rows; j++) 
                        {
                                if (i == j)
                                        continue;
                                phisum += phi(t, norm(atoms.row(j) - atoms.row(i)));
                        }
                        erep += repcoefs[n] * pow(phisum, n);
                }
        }
        return erep;
}

double electronic_energy(const vec& eig)
{
        return 2. * accu(eig.rows(0, eig.n_rows / 2 - 1));
}

double scaling(const tbparams& t, double r)
{
        return pow(t.r0 / r, t.n)
                * exp(t.n * (-pow(r / t.rc, t.nc)
                             + pow(t.r0 / t.rc, t.nc)));
}

mat construct_hamil(const mat& atoms,
                         const tbparams& t,
                         const config& conf)
{
        const index_t n = 4;
        index_t k = n * atoms.n_rows;
        if (conf.verbose > 20)
                std::cout << "Creating Hamiltonian, size = " << k << "\n";
        mat hamil(k, k);

        if (conf.verbose > 15)
                hamil.fill(4711.);

        // Since the Hamiltonian is symmetric, and I use an eigenvalue
        // solver for symmetric problems, one can calculate only the
        // upper triangular part (the Armadillo solver cannot use the
        // lower triangualar part).
        for (index_t j = 0; j < k; j++) 
        {
                for (index_t i = 0; i <= j; i++) 
                {
                        // Orbital indices within block
                        index_t ki = i % n;
                        index_t kj = j % n;
                        // Which block, i.e. which atoms
                        index_t bi = i / n;
                        index_t bj = j / n;
                        if (conf.verbose > 20) 
                        {
                                std::cout << "Block indices: " << bi << " " << bj;
                                std::cout << " Orbital indices: " << ki << " " << kj;
                                std::cout << " Indices: " << i << " " << j << "\n";
                        }
                        // Diagonal elements
                        if (i == j) 
                        {
                                if (ki == 0)
                                        hamil(i, i) = t.es;
                                else
                                        hamil(i, i) = t.ep;
                        }
                        // Off-diagonal elements
                        // Zero blocks for self-interaction
                        else if ((j - i > 0 && j - i <= n - ki - 1)
                                 && i != j)
                                hamil(i, j) = 0.0;
                        // s-s
                        else if (ki == 0 && kj == 0)
				hamil(i, j) = scaling(t,
                                                      norm(atoms.row(bj) -
							   atoms.row(bi))
					) * t.vsss;
                        // s-p and p-s
                        else if ((ki == 0 && kj > 0) || (kj == 0 && ki > 0)) 
                        {
                                double ll;
                                if (ki > 0)
                                        ll = -1.0;
                                else
                                        ll = 1.0;
                                rowvec v = atoms.row(bj) - atoms.row(bi);
                                double d = norm(v);
                                hamil(i, j) = ll * scaling(t, d) * t.vsps
                                        * v[std::max(ki, kj) - 1] / d;
                        }
                        // px-px, py-py, pz-pz
                        else if (ki > 0 && ki == kj) 
                        {
                                rowvec v = atoms.row(bj) - atoms.row(bi);
                                double d = norm(v);
                                double ll = pow(v[ki - 1] / d, 2);
                                hamil(i, j) = scaling(t, d)
                                        * (ll * t.vpps + (1 - ll) * t.vppp);
                        }
                        // px-py
                        else if (ki > 0 && kj > 0 && ki != kj) 
                        {
                                rowvec v = atoms.row(bj) - atoms.row(bi);
                                double d = norm(v);
                                double ll = v[ki - 1] / d * v[kj - 1] / d;
                                hamil(i, j) = scaling(t, d)
                                        * (ll * t.vpps - ll * t.vppp);
                        }
                }
        }
        // Fill in the lower triangular part
        if (conf.verbose > 16)
        {
                for (index_t j = 0; j < k; j++) 
                {
                        for (index_t i = j + 1; i < k; i++)
                                hamil(i, j) = hamil(j, i);
                }
        }
        if (conf.verbose > 15)
        {
                if (atoms.n_rows < 5)
                        std::cout << "Hamiltonian:\n" << std::fixed << hamil 
                                  << "\n";
                else 
                {
                        // std::ofstream of("hamil.txt");
                        // of << std::fixed << hamil << "\n";
                        // of.close();
                }
        }
        return hamil;
}


// Fortran "prototype"
// SUBROUTINE DSYEVR( JOBZ, RANGE, UPLO, N, A, LDA, VL, VU, IL, IU,
// $ ABSTOL, M, W, Z, LDZ, ISUPPZ, WORK, LWORK,
// $ IWORK, LIWORK, INFO )

extern "C" 
{
        void dsyevr_(const char *jobz, const char *range, const char *uplo,
                     int &n, double a[], int &lda,
                     double &vl, double &vu,
                     int &il, int &iu,
                     double &abstol, int &m, double w[],
                     double z[], int &ldz,
                     int isuppz[],
                     double work[], int &lwork,
                     int iwork[], int &liwork, int &info,
                     size_t jobz_len, size_t range_len, size_t uplo_len);
} 

vec solve_eigen_lapack(mat& hamil, const config& conf)
{
        int lwork, liwork;
        vec work(1);
        Col<int> iwork(1);
        int n = hamil.n_rows;
        vec eig(n);
        int m = 1;
        double tmp[1];
        int ltmp = 1;
        int isuppz[2];
        int info;
        double dz = 0.;
        int iz = 0;

        // Do a workspace query
        lwork = liwork = -1;
        dsyevr_("N", "A", "U", n, hamil.memptr(), n, dz, dz, iz, iz, dz, m,
                eig.memptr(), tmp, ltmp, isuppz, work.memptr(), lwork,
                iwork.memptr(), liwork, info, 1, 1, 1);
        if (info != 0)
                std::cout << "Lapack error during workspace query " << info << "\n";
        lwork = work[0];
        liwork = iwork[0];
        if (conf.verbose > 10) 
        {
                std::cout << "Optimal work array size " << lwork << "\n";
                std::cout << "Optimal integer work array size " << liwork << "\n";
        }
        work.resize(lwork);
        iwork.resize(liwork);

        // Do actual calculation
        dsyevr_("N", "A", "U", n, hamil.memptr(), n, dz, dz, iz, iz, dz, m,
                eig.memptr(), tmp, ltmp, isuppz, work.memptr(), lwork,
                iwork.memptr(), liwork, info, 1, 1, 1);
        if (info != 0)
                std::cout << "Lapack error " << info << "\n";
        return eig;
}

int main(void)
{
	trapfpe();
        config conf = read_config();
        std::vector<double> repcoef = read_repcoef(conf.repcoeffile);
        tbparams tbp = read_tbparams(conf.tbpfile);
        std::vector<std::string> systems = read_systems(conf.sysfile);

        for (std::vector<std::string>::const_iterator it = systems.begin();
             it != systems.end(); ++it) 
        {
                std::cout << "========================================\n";
                std::cout << "Calculating system " << *it << "\n";
                auto atoms = read_xyz(*it, conf);

                auto time1 = std::chrono::steady_clock::now();
                double rep = repulsive_energy(tbp, atoms, repcoef);
                auto time2 = std::chrono::steady_clock::now();
                if (conf.verbose > 5)
                {
                    auto diff = std::chrono::duration_cast
                        <std::chrono::duration<double>>(time2 - time1);
                    std::cout << "Calculating repulsive energy took "
                              << diff.count() << " s\n";
                }

                time1 = std::chrono::steady_clock::now();
                auto hamil = construct_hamil(atoms, tbp, conf);
		time2 = std::chrono::steady_clock::now();
                if (conf.verbose > 5) 
                {
			auto diff = std::chrono::duration_cast
				<std::chrono::duration<double>>(time2 - time1);
                        std::cout << "Constructing Hamiltonian took " 
                                  << diff.count() << " s\n";
                }

		time1 = std::chrono::steady_clock::now();
		vec eigval = eig_sym(hamil);
		time2 = std::chrono::steady_clock::now();
                if (conf.verbose > 5) 
                {
			auto diff = std::chrono::duration_cast
				<std::chrono::duration<double>>(time2 - time1);
                        std::cout << "Solving eigenvalues took "
                                  << diff.count() << " s.\n";
                }
                double eel = electronic_energy(eigval);

		untrapfpe();
		time1 = std::chrono::steady_clock::now();
                auto eigvals_lp = solve_eigen_lapack(hamil, conf);
		time2 = std::chrono::steady_clock::now();
		trapfpe();
                if (conf.verbose > 5) 
                {
			auto diff = std::chrono::duration_cast
				<std::chrono::duration<double>>(time2 - time1);
                        std::cout << "Solving eigenvalues with dsyevr took " 
                                  << diff.count() << " s.\n";
                }
                double eel_lp = electronic_energy(eigvals_lp);

                std::cout.precision(15);
                std::cout << "Eel = " << eel << "\n";
                std::cout << "Eel = " << eel_lp << " (LAPACK)\n";
                std::cout << "Erep = " << rep << "\n";
                std::cout << "Etot = " << eel + rep << "\n";
                std::cout << "Eb/N = " << (eel + rep) / atoms.n_rows
                        + 1.150976511819100 << "\n";
        }
}
