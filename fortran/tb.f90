!****h* /tb
! COPYRIGHT
!  Copyright (c) 2006 Janne Blomqvist
! PURPOSE
! Simple Tight Binding code.
!****
program tb

  use iso_fortran_env, only: int64
  implicit none

  integer, parameter :: &
       wp = selected_real_kind (15) ! Working precision.

  ! Config parameters, and their default values.
  integer :: verbose = 2
  character(len=256) :: sysfile = 'systems.txt', repcoeffile = 'repcoef.txt', &
       tbpfile = 'tbparams.txt'

  character(len=25), allocatable :: systems(:)

  real(wp), allocatable :: atoms(:,:), repcoef(:), hamil(:,:), eig(:)

  real(wp) :: erep, eel
  integer(int64) :: t2, t1, cr
  integer :: isys

  ! Tight binding params
  type tbp_t
     real(wp) :: es, ep, vsss, vsps, vpps, vppp, n, nc, rc, r0, r1, &
          phi0, m, mc, dc, d0, d1
  end type tbp_t

  type(tbp_t) :: tbp
  integer :: tbp_n

  call read_config ()
  do isys = 1, size (systems)
     write (*,*) '==============================='
     write (*,*) 'Calculating system ', systems(isys)
     call read_xyz (systems(isys))
     call system_clock (t1, cr)
     call repulsive_energy (erep)
     call system_clock (t2)
     if (verbose > 1) then
        write (*,*) 'Calculating repulsive energy took ', real(t2 - t1, wp) / cr, ' s.'
     end if
     call system_clock (t1)
     call construct_hamil ()
     call system_clock (t2)
     if (verbose > 1) then
        write (*,*) 'Constructing Hamiltonian took ', real(t2 - t1, wp) / cr, ' s.'
     end if
     allocate (eig(size (hamil, 1)))
     call system_clock (t1)
     call solve_eigen (eig)
     call system_clock (t2)
     if (verbose > 1) then
        write (*,*) 'Solving eigenvalues took ', real(t2 - t1, wp) / cr, ' s.'
     end if
     eel = electronic_energy (eig)
     write (*,*) 'Eel  = ', eel
     write (*,*) 'Erep = ', erep
     write (*,*) 'Etot = ', erep + eel
     write (*,*) 'Eb/N = ', (erep + eel) / real (size (atoms,2), wp) &
          + 1.150976511819100_wp
     deallocate (eig)
  end do

contains

  !****f* tb/read_config
  ! PURPOSE
  ! Read config data with namelist.
  !****
  subroutine read_config ()
    integer :: lun, istat, cind, i
    character(len=256) :: line, key, val

    open (newunit=lun, file='tb.conf', form='formatted', status='old', action='read')
    do
       read (lun, '(A)', iostat=istat) line
       if (istat /= 0) exit
       ! Remove comments
       cind = 0
       do i = 1, len_trim(line)
          if (line(i:i) == '#') exit
          cind = i
       end do
       if (cind <= 2) cycle
       line = line(1:cind)
       ! Split line on =
       do i = 1, len_trim(line)
          if (line(i:i) == '=') then
             key = trim(adjustl(line(1:(i - 1))))
             val = trim(adjustl(line((i + 1):len_trim(line))))
             exit
          end if
       end do
       if (key == 'sysfile') then
          sysfile = val
       else if (key == 'repcoeffile') then
          repcoeffile = val
       else if (key == 'tbpfile') then
          tbpfile = val
       else if (key == 'verbose') then
          read(val, *) verbose
       end if
    end do
    close (lun)

    call read_data ()
  end subroutine read_config


  !****f* tb/read_data
  ! PURPOSE
  ! Read data from files.
  !****
  subroutine read_data ()
    integer :: nrepcoef, nsys

    open (11, file=sysfile, form='formatted', status='old', action='read')
    ! First line contains number of systems to calculate
    read (11, '(I99)') nsys
    allocate (systems(nsys))
    read (11, '(A)') systems
    close (11)

    ! Read coefficients for the repulsion function.
    open (11, file=repcoeffile, form='formatted', status='old', action='read')
    ! First line contains the number of coefficients
    read (11, '(I99)') nrepcoef
    allocate (repcoef(0:nrepcoef-1))
    read (11, '(F20.17)') repcoef
    close (11)
    if (verbose > 25) then
       write (*,*) 'Repulsion coefficients: ', repcoef
    end if

    ! Read TB parameters
    open(13, file=tbpfile, form='formatted', status='old', action='read')
    read (13, *) tbp
    close (13)
    if (verbose > 25) then
       write (*, *) 'TB parameters:'
       write (*, *) tbp
    end if
    tbp_n = int(tbp%n)
  end subroutine read_data


  !****f* tb/read_xyz
  ! PURPOSE
  ! Read a .xyz file.
  !****
  subroutine read_xyz (infile)
    character(len=*), intent(in) :: infile
    integer, parameter :: xyz_iu = 12
    integer :: natoms, i
    character(len=5) :: symbol

    open (unit=xyz_iu, file=infile, form='formatted', &
         access='sequential', action='read', status='old')
    read (xyz_iu, *) natoms
    read (xyz_iu, '(A)') ! Comment line
    if (allocated (atoms)) then
       deallocate (atoms)
    end if
    allocate (atoms(3, natoms))
    do i = 1, natoms
       ! Discard the chemical symbol as we don't need it.
       read (xyz_iu, *) symbol, atoms(:,i)
    end do
    close (xyz_iu)
    if (verbose > 10 .and. size (atoms, 2) < 10) then
       write (*,*) 'Coordinates:'
       do i = 1, natoms
          write (*,*) atoms(:,i)
       end do
    end if
  end subroutine read_xyz


  !****f* tb/dist
  ! PURPOSE
  ! The distance between two atoms.
  !****
  pure function dist (i, j)
    integer, intent(in) :: i, j
    real(wp) :: dist, dv(3)
    dv = atoms(:, i) - atoms(:, j)
    dist = sqrt (dot_product (dv, dv))
  end function dist


  !****f* tb/phi
  ! PURPOSE
  ! Function phi(r)
  !****
  pure function phi (r)
    real(wp), intent(in) :: r
    real(wp) :: phi
    phi = tbp%phi0 * (tbp%d0 / r)**tbp%m * exp (tbp%m * &
         (- (r / tbp%dc)**tbp%mc + (tbp%d0 / tbp%dc)**tbp%mc))
  end function phi


  !****f* tb/repulsive_energy
  ! PURPOSE
  ! Calculate the repulsive energy
  !****
  subroutine repulsive_energy (erep)
    real(wp), intent(out) :: erep
    integer :: i, j, n
    real(wp) :: phisum
    erep = 0.0_wp
    do i = 1, size (atoms, 2)
       do n = lbound (repcoef, 1), ubound (repcoef, 1)
          phisum = 0.0_wp
          do j = 1, size (atoms, 2)
             if (i == j) then
                cycle
             end if
             phisum = phisum + phi (dist (i,j))
          end do
          erep = erep + repcoef(n) * phisum**(n)
       end do
    end do
    erep = erep
  end subroutine repulsive_energy


  !****f* tb/electronic_energy
  ! PURPOSE
  ! Calculate the electronic energy.
  ! NOTES
  ! The number of occupied orbitals is
  !****
  pure function electronic_energy (eig)
    real(wp), intent(in) :: eig(:)
    real(wp) :: electronic_energy
    electronic_energy = 2.0_wp * sum (eig(:size(eig)/2))
  end function electronic_energy


  !****f* tb/scaling
  ! PURPOSE
  ! The scaling function for the matrix elements.
  !****
  pure function scaling (r)
    real(wp), intent(in) :: r
    real(wp) :: scaling
    scaling = (tbp%r0 / r)**tbp_n * exp (tbp_n * (- (r / tbp%rc)**tbp%nc &
         + (tbp%r0 / tbp%rc)**tbp%nc))
  end function scaling


  !****f* tb/construct_hamil
  ! PURPOSE
  ! Construct the Hamiltonian matrix. Only works for sp-orbitals, with
  ! homogeneous atoms.
  !****
  subroutine construct_hamil ()
    integer :: i, j, ki, kj, bi, bj
    integer, parameter :: n = 4 ! Orbitals per atom; 1s, 3p
    real(wp) :: ll
    i = n * size (atoms, 2)
    if (allocated (hamil)) then
       deallocate (hamil)
    end if
    allocate (hamil(i, i))
    !hamil = 4711.0_wp ! check that we set every element.
    do j = 1, size (hamil, 1)
       ! Since the Hamiltonian is symmetric, and I use an eigenvalue solver
       ! for symmetric problems, there is no need to calculate the lower 
       ! triangular part.
       do i = 1, j
          ! Which element in a block
          ki = mod (i, n)
          kj = mod (j, n)
          if (ki == 0) then
             ki = n
          end if
          if (kj == 0) then
             kj = n
          end if
          ! Which block
          bi = ceiling (real(i,wp)/real(n,wp))
          bj = ceiling (real(j,wp)/real(n,wp))
          ! Diagonal elements.
          if (i == j) then
             if ( ki == 1) then
                hamil(i,i) = tbp%es
             else
                hamil(i,i) = tbp%ep
             end if
          ! Off-diagonal elements
          ! Zero blocks for self-interaction.
          else if ((j-i > 0 .and. j-i <= n-ki) & ! upper triangular part
               !.or. (i-j > 0 .and. i-j < ki)) & ! lower triangular
               .and. i /= j) then ! diagonal
             hamil(i,j) = 0.0_wp
          ! s-s
          else if (ki == 1 .and. kj == 1) then
             hamil(i,j) = scaling (dist (bi,bj)) * tbp%vsss
          ! s-p and p-s
          else if (ki == 1 .and. kj > 1 .or. kj == 1 .and. ki > 1) then
             if (ki > 1) then
                ll = -1.0_wp
             else
                ll = 1.0_wp
             end if
             !write (*,*) 'dir_cos ', bi, bj, ki, kj, dir_cos (bi,bj,max(ki,kj)-1)
             hamil(i,j) = ll * scaling (dist (bi, bj)) * tbp%vsps &
                  * dir_cos (bi, bj, max (ki, kj) - 1)
          ! px-px (and py-py, pz-pz)
          else if (ki > 1 .and. ki == kj) then
             ll = dir_cos (bi, bj, ki - 1) ** 2
             ! write (*,*) 'px-px l: ', ll, ki
             hamil(i,j) = scaling (dist (bi, bj)) * (ll * tbp%vpps &
                  + (1 - ll) * tbp%vppp)
          ! px-py
          else if (ki > 1 .and. kj > 1 .and. ki /= kj) then
             ll = dir_cos (bi, bj, ki - 1) * dir_cos (bi, bj, kj - 1)
             hamil(i,j) = scaling (dist (bi, bj)) * (ll * tbp%vpps &
                  - ll * tbp%vppp)
          end if
       end do
    end do
    if (verbose > 5) then
       ! Fill in the lower triangular part so we can output the thing.
       do j = 1, size (hamil, 1)
          do i = j+1, size (hamil, 2)
             hamil(i,j) = hamil(j,i)
          end do
       end do
       if (size (atoms, 2) < 4) then
          do i = 1, size (hamil, 1)
             write (*,'(99(F8.4,1X))') hamil(i,:)
          end do
       else
          open (13, file="hamil", form='formatted', action='write')
          do i = 1, size (hamil, 1)
             write (13, '(9999(F8.4,1X))') hamil(i,:)
          end do
          close (13)
       end if
    end if
  end subroutine construct_hamil


  !****f* tb/dir_cos
  ! PURPOSE
  ! Directional cosine.
  ! INPUTS
  ! dir - The index of the basis vector to calculate the directional
  !       cosine for. I.e. x = 1, y = 2, z = 3.
  !****
  pure function dir_cos (i, j, dir)
    integer, intent(in) :: i, j, dir
    real(wp) :: dv(3), dir_cos
    dv = atoms(:,j) - atoms(:,i)
    dir_cos = dv(dir) / dist (i, j)
  end function dir_cos


  !****f* tb/solve_eigen
  ! PURPOSE
  ! Interface to the eigenvalue solver. (dsyevr from lapack)
  ! Returns an array with all the real eigenvalues.
  !****
  subroutine solve_eigen (eig)
    real(wp), intent(out), dimension(:) :: eig
    real(wp), allocatable :: work(:)
    real(wp) :: tmp(1,1)
    integer :: lwork, liwork
    integer, allocatable :: iwork(:)
    integer :: info, n, m, isuppz(2)
    external :: dsyevr
    
    n = size (hamil, 1)
    m = 1

    if (.not. allocated (work)) then
       ! Do a workspace query to determine the optimal work array size.
       lwork = -1
       liwork = -1
       allocate (work(1), iwork(1))
       call dsyevr ('N', 'A', 'U', n, hamil, n, 0.0_wp, 0.0_wp, 0, 0, 0.0_wp, m, &
            eig, tmp, 1, isuppz, work, lwork, iwork, liwork, info)
       if (info /= 0) then
          write (*,*) 'Lapack error during workspace query: ', info
       end if
       lwork = nint (work(1))
       liwork = iwork(1)
       if (verbose > 20) then
          write (*,*) 'Optimal work array size is: ', lwork
          write (*,*) 'Optimal integer work array size: ', liwork
       end if
       deallocate (work, iwork)
       allocate (work(lwork), iwork(liwork))
    end if

    call dsyevr ('N', 'A', 'U', n, hamil, n, 0.0_wp, 0.0_wp, 0, 0, 0.0_wp, m, eig, &
            tmp, 1, isuppz, work, lwork, iwork, liwork, info)

    if (info /= 0) then
       write (*,*) 'Lapack error: ', info
    end if
    
  end subroutine solve_eigen


end program tb

