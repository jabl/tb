#!/usr/bin/env julia

struct Tbp
  es::Float64
  ep::Float64
  vsss::Float64
  vsps::Float64
  vpps::Float64
  vppp::Float64
  n::Int
  nc::Float64
  rc::Float64
  r0::Float64
  r1::Float64
  phi0::Float64
  m::Float64
  mc::Float64
  dc::Float64
  d0::Float64
  d1::Float64
end

function read_config(stream)
    #"""Read the config"""
    sysfile = ""
    repcoeffile = ""
    tbpfile = ""
    verbose = 100
    for line in eachline(stream)
        s = strip(split(line, "#")[1])
        if length(s) <= 1
            continue
        end
        s = split(s, "=")
        key = strip(s[1])
        val = strip(s[2])
        if key == "sysfile"
            sysfile = val
        elseif key == "repcoeffile"
            repcoeffile = val
        elseif key == "tbpfile"
            tbpfile = val
        elseif key == "verbose"
            verbose = parse(Int, val)
        else
            println("Invalid key in line: $line")
        end
    end
    systems = readlines(sysfile)
    systems = systems[2:end]
    rc = readlines(repcoeffile)
    rc = rc[2:end]
    r = map(x -> parse(Float64, x), rc)
    t = readlines(tbpfile)
    t = map(x -> parse(Float64, x), t)
    tbp = Tbp(t[1], t[2], t[3], t[4], t[5], t[6], Int(t[7]), t[8], t[9],
              t[10], t[11], t[12], t[13], t[14], t[15], t[16], t[17])
    return systems, r, tbp, verbose
end

function parse_xyz(stream)
  # Parse XYZ from a stream
  natoms = parse(Int, readline(stream))
  readline(stream)
  a = Array{Float64}(undef, (3, natoms))
  for l = 1:size(a,2)
    s = split(readline(stream))
    for d = 1:3
      a[d, l] = parse(Float64, s[d + 1])
    end
  end
  return a
end

function read_xyz(fname)
  #"""Parse an xyz file"""
  return open(parse_xyz, fname)
end

function phi(t, r)
  return (t.phi0 * (t.d0 / r) ^ t.m
  * exp(t.m * (-(r / t.dc) ^ t.mc
  + (t.d0 / t.dc) ^ t.mc)))
end


function repulsive_energy(tbp, atoms, r)
  erep = 0.
  # Preallocate array v
  v = Array{Float64}(undef, 3)
  for ii = 1:size(atoms, 2)
    for n = 1:length(r)
      phisum = 0.
      for jj = 1:size(atoms, 2)
        if ii == jj
          continue
        end
        # Devectorize loop to avoid allocation+gc overhead
        @views @. v[:] = atoms[:, jj] - atoms[:, ii]
        # ... and devectorize loop (manually)
        #@inbounds @simd for k = 1:3
        #  v[k] = atoms[k, jj] - atoms[k, ii]
        #end
        d = norm(v)
        phisum += phi(tbp, d)
      end
      erep += r[n] * phisum ^ (n - 1)
    end
  end
  return erep
end

function scaling(t, r)
  return ((t.r0 / r) ^ t.n
  * exp(t.n * (-(r / t.rc) ^ t.nc
  + (t.r0 / t.rc) ^ t.nc)))
end

using LinearAlgebra

function construct_hamil(atoms, t, verbose)
  #"""Construct the Hamiltonian"""
  n = 4
  k = n * size(atoms, 2)
  hamil = Array{Float64}(undef, k, k)
  #hamil = Symmetric(Float64, k, k)
  if verbose > 15
    hamil[:,:] = 4711.
  end
  # Preallocate tmp array for a coordinate
  v = Array{Float64}(undef, 3)
  # Hamiltonian is symmetric, fill only upper triangular part
  for jj in 1:size(hamil, 1)
    for ii in 1:jj
      # Orbital indices within block
      ki = ii % n
      if ki == 0
        ki = n
      end
      kj = jj % n
      if kj == 0
        kj = n
      end
      # Which block
      bi = round(Int, cld(ii, n))
      bj = round(Int, cld(jj, n))
      if verbose > 20
        println("Block indices: ", bi, bj)
        println("Orbital indices: ", ki, kj)
        println("Indices: ", ii, jj)
      end
      # Diagonal elements
      if ii == jj
        if ki == 1
          hamil[ii, ii] = t.es
        else
          hamil[ii, ii] = t.ep
        end
        # Off-diagonal elements
        # zero blocks for self-interaction
      elseif (jj - ii) > 0 && jj - ii <= n - ki && ii != jj
        hamil[ii, jj] = 0.
        # s-s
      elseif ki == 1 && kj == 1
          @views @. v[:] = atoms[:, bj] - atoms[:, bi]
          #@inbounds @simd for z = 1:3
          #    v[z] = atoms[z, bj] - atoms[z, bi]
          #end
        d = norm(v)
        hamil[ii, jj] = scaling(t, d) * t.vsss
        # s-p and p-s
      elseif (ki == 1 && kj > 1) || (kj == 1 && ki > 1)
        if ki > 1
          ll = -1
        else
          ll = 1
        end
          @views @. v[:] = atoms[:, bj] - atoms[:, bi]
          #@inbounds @simd for z = 1:3
          #    v[z] = atoms[z, bj] - atoms[z, bi]
          #end
        d = norm(v)
        hamil[ii, jj] = (ll * scaling(t, d) * t.vsps
        * v[max(ki, kj) - 1] / d)
        # px-px, py-py, pz-pz
      elseif ki > 1 && ki == kj
          @views @. v[:] = atoms[:, bj] - atoms[:, bi]
          #@inbounds @simd for z = 1:3
          #    v[z] = atoms[z, bj] - atoms[z, bi]
          #end
        d = norm(v)
        ll = (v[ki - 1] / d) ^ 2
        hamil[ii, jj] = (scaling(t, d)
        * (ll * t.vpps + (1 - ll) * t.vppp))
        # px-py
      elseif (ki > 1 && kj > 1 && ki != kj)
          @views @. v[:] = atoms[:, bj] - atoms[:, bi]
          #@inbounds @simd for z = 1:3
          #    v[z] = atoms[z, bj] - atoms[z, bi]
          #end
        d = norm(v)
        ll = v[ki - 1] / d * v[kj - 1] / d
        hamil[ii, jj] = (scaling(t, d)
        * (ll * t.vpps - ll * t.vppp))
      end
    end
  end
  hamil = Symmetric(hamil)
  # Fill in lower triangular part
  #if verbose > 0
  #  for jj = 1:k
  #    for ii = jj+1:k
  #      hamil[ii, jj] = hamil[jj, ii]
  #    end
  #  end
  #end
  if verbose > 15
    if size(atoms, 2) < 5
      println("Hamiltonian:\n", hamil, "\n")
    else
      f = open("hamil.txt", "w")
      write(f, full(hamil))
      close(f)
    end
  end
  return hamil
end


function electronic_energy(heig)
  return 2 * sum(heig[1:div(length(heig), 2)])
end

function run_main()
  systems, r, tbparams, verbose = open(read_config, "tb.conf")
  for sys in systems
    println("==============================================================")
    println("Calculating system ", sys)
    println("Reading data from XYZ file")
    @time atoms = read_xyz(sys)
    println("Calculating repulsive energy")
    @time rep = repulsive_energy(tbparams, atoms, r)
    println("Constructing Hamiltonian")
    @time hamil = construct_hamil(atoms, tbparams, verbose)
    println("Calculating eigenvalues")
    @time h_eigs = eigvals(hamil)
    println("Calculating electronic energy")
    @time eel = electronic_energy(h_eigs)
    println("Eel = ", eel)
    println("Erep = ", rep)
    println("Etot = ", rep + eel)
    println("Eb/N = ", (rep + eel) / size(atoms, 2) + 1.150976511819100)
  end
end

run_main()
